import Vue from 'vue'
import axios from 'axios'
import App from './App'
import router from './pages/index'
import { store } from './store'
import Admin from '@/components/settingLayout/settingLayout.vue'
import { BACKGROUND_PATH } from '@/common/config'

Vue.prototype.$EventBus = new Vue();
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.component('setting-layout', Admin);
Vue.mixin({
  methods: {
    getFileName() {
      const {dialog, BrowserWindow, getCurrentWindow} = require('electron').remote;
      const child = new BrowserWindow({parent: getCurrentWindow(), modal: true, show: false});
      const file = dialog.showOpenDialog(child, {properties: ['openFile']});
      if (!file) return;
      return file[0];
    },
    getRouterName() {
      return this.$router.currentRoute.path.replace('/', '').replace('-setting','');
    },
    objectReplaceToStyle(item) {
      const stringfy = JSON.stringify(item);
      let result = stringfy.replace(/,/gi,';');
      result = result.replace(/\"/gi,'');
      return result;
    },
    getBackground(name) {
      this.$store.state.mirroring.isAllBackgroun ? 
      { background: `url(${ BACKGROUND_PATH }${ this.$store.state.mirroring.background }) no-repeat 0 0 / contain` }
      : { background: `url(${ BACKGROUND_PATH }${ this.$store.state[name].background }) no-repeat 0 0 / contain` }
    },
  },
})

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
