
import { PASSWORD_URL } from '../../common/config'

const fs = require('fs');

export const writePassword = function(password) {
  fs.writeFile(PASSWORD_URL, password, () => {
    console.log('Password Export Success');
  })
}
