
import { FRAME_PATH } from '../../common/config'

const fs = require('fs');


export const copyFrameFile = function(dir) {
  const path = FRAME_PATH;
  const fileSplit = dir.split('\\');
  try {
    fs.copyFileSync(dir, `${ path }${ fileSplit[fileSplit.length-1] }`);
  }catch(e) {
  }
  
}
