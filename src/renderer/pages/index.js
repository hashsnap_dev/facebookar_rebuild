import Vue from 'vue'
import Router from 'vue-router'
import main from './main.vue'
import adminLogin from './adminLogin/adminLogin.vue'
import print from './print.vue'
import MirrorSetting from './mirroringSetting/mirroringSetting.vue';
import PhotoFrameSelectSetting from './photoFrameSelectSetting/photoFrameSelectSetting.vue';
import EmailSetting from './emailSetting.vue';
import PrintSetting from './printSetting/printSetting.vue';
import PasswordSetting from './passwordSetting/passwordSetting.vue';



Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/main',
      name: 'main',
      component: main,
    },
    {
      path: '/adminLogin',
      name: 'adminLogin',
      component: adminLogin,
    },
    {
      path: '/print',
      name: 'print',
      component: print,
    },
    {
      path: '/mirroring-setting',
      component: MirrorSetting,
    },
    {
      path: '/photoFrame-setting',
      component: PhotoFrameSelectSetting,
    },
    {
      path: '/email-setting',
      component: EmailSetting,
    },
    {
      path: '/print-setting',
      component: PrintSetting,
    },
    {
      path: '/password-setting',
      component: PasswordSetting,
    },
    {
      path: '*',
      redirect: '/main'
    },
  ]
})
