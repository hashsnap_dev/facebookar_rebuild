
import { SETTING_DIR, COMMON_URL } from '../../common/config'

const fs = require('fs');
const fse = require('fs-extra');

export const readJson = function(dir) {  // 파일읽기
  importSetting(dir)
  return JSON.parse(fs.readFileSync(dir)); 
}

export const defaultJson = function() {
  return JSON.parse(fs.readFileSync(`${ COMMON_URL }setting.json`));
}

const importSetting = function(dir) {

  let importPath = dir.split('\\'); // 요기는 세팅 이미지 프레임 등 다른것도 불러오는것
  let resultPath = '';
  
  for (let i = 0; i < importPath.length - 1 ; i++) {
    resultPath += importPath[i]+'\\';
  }
  fse.copy(resultPath, SETTING_DIR, (err) => {
    console.log('import success', resultPath);
  })
}