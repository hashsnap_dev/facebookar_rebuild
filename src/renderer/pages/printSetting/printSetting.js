
import { VIDEO_PATH } from '../../common/config'

const fs = require('fs');

export const copyFile = function(dir) {
  const path = VIDEO_PATH;
  const fileSplit = dir.split('\\');
  try {
    fs.copyFileSync(dir, `${ path }${ fileSplit[fileSplit.length-1] }`);
  }catch(e) {
  }
}
