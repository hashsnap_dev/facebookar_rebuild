export default class startDrag {
  constructor(obj, { range: {
      x,
      y,
      w,
      h
  }
  }) {
      this.img_L = 0;
      this.img_T = 0;
      this.put = false;
      this.targetObj = obj;
      this.left = this.getLeft(this.targetObj);
      this.top = this.getTop(this.targetObj);
      this.minX = x // this.left; 
      this.minY = y // this.top;
      this.maxX = this.minX + w;
      this.maxY = this.minY + h;
      obj.addEventListener('mousedown', (e) => this.start1(e));
      document.addEventListener('mousemove', (e) => this.moveDrag(e));
      document.addEventListener('mouseup', () => this.stopDrag());
      obj.addEventListener('touchstart', (e) => this.start1(e));
      document.addEventListener('touchmove', (e) => this.moveDrag(e));
      document.addEventListener('touchend', () => this.stopDrag());;
  }

  start1(e) {
      e.preventDefault();
      const event = e.changedTouches? e.changedTouches[0] : e
      this.img_L = this.left - event.clientX;
      this.img_T = this.top - event.clientY;
      this.put = true;
  }

  getLeft(o) {
      if (o.style.left == "") {
          o.style.left = "0px";
      }
      return parseInt(o.style.left.replace('px', ''));
  }

  getTop(o) {
      if (o.style.top == "") {
          o.style.top = "600px";
      }
      return parseInt(o.style.top.replace('px', ''));
  }

  moveDrag(e) {
      if (!this.put) return;
    //   let e_obj = window.event ? window.event : event;
      let e_obj = e.changedTouches? e.changedTouches[0] : e;
      let dmvx = e_obj.clientX + this.img_L;
      let dmvy = e_obj.clientY + this.img_T;
      if (dmvx < this.minX) dmvx = this.minX;
      if (dmvx > this.maxX) dmvx = this.maxX;
      if (dmvy < this.minY) dmvy = this.minY;
      if (dmvy > this.maxY) dmvy = this.maxY;
      this.left = dmvx;
      this.top = dmvy;
      this.targetObj.style.left = dmvx + "px";
      this.targetObj.style.top = dmvy + "px";
      return false;
  }

  stopDrag() {
      this.put = false;
  }
}



// new startDrag('', {
//   range: {
//       x: 0,
//       y: 0,
//       w: 0,
//       h: 1920,
//   }
// });
