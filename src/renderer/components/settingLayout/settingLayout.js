
import { BACKGROUND_PATH, SETTING_DIR } from '../../common/config'

const fs = require('fs');
const fse = require('fs-extra');


export const writeJson = function(data) {
  const now = new Date();
  const realJsonName = `${ now.getFullYear() }-${ now.getMonth()+1 }-${ now.getDate() }-${ now.getHours() }-${ now.getUTCMinutes() }-${ now.getUTCSeconds() }.json`;
  const JSON_DIR = `${ SETTING_DIR }${ realJsonName }`;
  console.log(JSON_DIR);
  fs.writeFile(JSON_DIR, JSON.stringify(data), () => {
    console.log('Setting Export Success');
    exportSetting();
  })
}

export const copyBackgroundFile = function(dir) {
  const path = BACKGROUND_PATH;
  const fileSplit = dir.split('\\');
  try {
    fs.copyFileSync(dir, `${ path }${ fileSplit[fileSplit.length-1] }`);
  }catch(e) {
  }
}

const exportSetting = function() {
  const PATH = 'C:\\setting';
  
  fse.copy(SETTING_DIR, PATH, (err) => {
    console.log('export success');
  })
}
