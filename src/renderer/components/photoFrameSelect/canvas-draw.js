export const drawImageCover = function (ctx, img, x, y, w, h, offsetX, offsetY) {

  if (arguments.length === 2) {
      x = y = 0;
      w = ctx.canvas.width;
      h = ctx.canvas.height;
  }
  const Magnification = 1;

  offsetX = typeof offsetX === "number" ? offsetX : 0.5;
  offsetY = typeof offsetY === "number" ? offsetY : 0.5;

  if (offsetX < 0) offsetX = 0;
  if (offsetY < 0) offsetY = 0;
  if (offsetX > 1) offsetX = 1;
  if (offsetY > 1) offsetY = 1;

  let iw = img.width, // iw 의 i 는 image 의 i
      ih = img.height,
      r = Math.min(w / iw, h / ih),
      nw = iw * r,   // new prop. width
      nh = ih * r,   // new prop. height
      cx, cy, cw, ch, ar = 1;

  if (nw < w) ar = w / nw;                             
  if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh; 
  nw *= ar;
  nh *= ar;

  cw = iw / (nw / w) * Magnification;
  ch = ih / (nh / h) * Magnification;

  cx = (iw - cw) * offsetX;
  cy = (ih - ch) * offsetY;

  if (cx < 0) cx = 0;
  if (cy < 0) cy = 0;
  if (cw > iw) cw = iw;
  if (ch > ih) ch = ih;

  ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);
}


export const drawImageContain = function (ctx, img, x, y, w, h) {

    if (arguments.length === 2) {
        x = y = 0;
        w = ctx.canvas.width;
        h = ctx.canvas.height;
    }
  
    offsetX = typeof offsetX === "number" ? offsetX : 0.5;
    offsetY = typeof offsetY === "number" ? offsetY : 0.5;
  
    if (offsetX < 0) offsetX = 0;
    if (offsetY < 0) offsetY = 0;
    if (offsetX > 1) offsetX = 1;
    if (offsetY > 1) offsetY = 1;
  
    let iw = img.width, // iw 의 i 는 image 의 i
        ih = img.height,
        iw_ih_r = iw / ih,
        r = Math.max(w / iw, h / ih), // 비율
        nw = iw * r,   // new prop. width // 이미지의 폭과 높이를 비율을 곱하여 줄인 값
        nh = ih * r,   // new prop. height
        cw, ch, ar = 1;
  
    if (nw < w) ar = w / nw;                             
    if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh; 
    nw *= ar;
    nh *= ar;


    cw = iw / (nw / w) ;
    ch = ih / (nh / h) ;
    

    cw = cw * r;
    ch = ch * r;

    if (iw_ih_r > 1) {
        ch = ch / iw_ih_r;
    }else {
        cw = cw * iw_ih_r;
    }

    
    ctx.drawImage(img, 0, 0, iw, ih,  x, y, cw, ch);
  }
  

  export const getFramePosition = function(ctx, frameImage) { // 캔버스 빈공간 (직사각형) 좌표찾는 메소드
    const canvas = document.createElement('canvas');
    canvas.width = ctx.canvas.width;
    canvas.height = ctx.canvas.height;
    const invisibleCtx = canvas.getContext('2d');

        invisibleCtx.drawImage(frameImage, 0, 0);

        const raster = invisibleCtx.getImageData(0, 0, invisibleCtx.canvas.width, invisibleCtx.canvas.height).data;
        let x1 = ctx.canvas.width,
        y1 = ctx.canvas.height,
        x2 = 0,
        y2 = 0,
        canvas_width = ctx.canvas.width,
        canvas_height = ctx.canvas.height;
        
        // w = point % canvas_width;  이게 x좌표값
        // h = floor(point / canvas_height) 이게 y좌표값

        for (let i = 3; i < raster.length; i+=4) { // 여기서 top은 금방 구해지고,
        const point = (i-3)/4;
        if (raster[i] == 0){
        y1 = y1 < Math.floor(point / canvas_width) ? y1 : Math.floor(point / canvas_width);

        x1 = x1 < point % canvas_width ? x1 : point % canvas_width;

        x2 = x2 < point % canvas_width ? point % canvas_width : x2;

        y2 = y2 < Math.floor(point / canvas_width) ? Math.floor(point / canvas_width) : y2;
        }
        }

        x2 = x2 - x1 + 1;
        y2 = y2 - y1 + 1;
        return {'x1': x1, 'x2': x2, 'y1': y1, 'y2': y2};
        
  }

  export const rotateImage = function (ctx, canvas, image) { // 이미지 회전
    const x = canvas.width / 2;
    const y = canvas.height / 2;
    const width = image.width;
    const height = image.height;
    const RADIAN = Math.PI/180;
    const angleInRadians = 90 * RADIAN;
    ctx.translate(x, y);
    ctx.rotate(angleInRadians);
    ctx.drawImage(image, -width / 2, -height / 2, width, height);
    ctx.rotate(-angleInRadians);
    ctx.translate(-x, -y);
  }