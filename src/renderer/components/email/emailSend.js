const fs = require('fs');
const request = require('request');
import { RESULTIMAGE_DIR, FILEUPLOAD_URL } from '@/common/config'

export function sendEmailBefore(user_email, image_url) {
  console.log(user_email, image_url);
  const formData = {
    file: fs.createReadStream( RESULTIMAGE_DIR + image_url ),
  };


  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      url: FILEUPLOAD_URL,
      formData: formData,
    },(err, httpResponse, body) => {
      console.log(err, body);
      if (err) reject();
      resolve(body);
    });
  })
}

export function sendEmail(user_email, imgUrl) {
  const request = require('request');
  const fromName = 'test Mirror';
  const fromAddress = 'fb.camera.effects@hashsnap.net'
  const toAddress = user_email;
  const subject = 'Mail test';
  const mailContext = 'Hello E-mail';
  
  const email_re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (email_re.test(toAddress)) {
    return new Promise((resolve, reject) => {
      request.post({
        headers: { 'Content-Type': 'application/json;' },
        url: 'https://m4na165xf4.execute-api.ap-northeast-2.amazonaws.com/api/raw',
        body: JSON.stringify({
        data: `From: ${ fromName } <${ fromAddress }>
To: ${ toAddress }
Subject: ${ subject } 
MIME-Version: 1.0
Content-Type: text/html; charset=utf-8

${ mailContext }
${ imgUrl }`
              })
        }, (err, res, body) => {
              if (err) reject();
              resolve();
              // return Promise.resolve('전송에 성공했습니다.');
              // callback('E-mail transfer completed.')
            }
          );
    })
    }else{
      reject();
    }
  }