import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const mutations = {
  setOption(state, payload) {
    console.log(payload);
    state[payload.name][payload.key] = payload.value;
  },
  setStyle(state, payload) {
    console.log(payload);
    state[payload.name][payload.key][payload.style] = payload.value;
  },
  setState(state, payload) {
    const nameArr = ['mirroring', 'photoFrame', 'email', 'print', 'password'];
    nameArr.forEach((element) => {
      state[element] =  payload[element];
    })
  }
}
const state = {
  mirroring: {
    background: 'default-bg.png',
    isAllBackground: false,
    customCss: '',
  },
  photoFrame: {
    background: 'default-bg.png',
    customCss: '',
    frames: ['photo-frame-single.png'],
    privious_button: {
      background: '#ffffff',
      color: '#4762AB',
    },
    export_button: {
      background: '#4762AB',
      color: '#ffffff',
    },
    frame_arrow: {
      color: '#4762AB',
    },
  },
  email: {
    background: 'default-bg.png',
    customCss: '',
    back_button: {
      background: '#a0a0a0',
      color: '#000000',
    },
    send_button: {
      background: '#ff4040',
      color: '#ffffff',
    },
  },  
  print: {
    background: 'default-bg.png',
    customCss: '',
    advertisement: 'video-0.mp4',
    video: {
      width: '100%',
      top: '0px',
      left: '0px',
    },
    video_bar: {
      background: '#ff0000',
      display: 'block',
    },
  },
  password: {
    background: 'default-bg.png',
    customCss: '',
  },
}
const getters = {}


export const store = new Vuex.Store({
  mutations,
  state,
  getters,
})
