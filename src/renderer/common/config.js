const path = require('path');

const env = process.env.NODE_ENV == 'development' ? true : false;


let PHOTO_FRAME_URL, MYIMAGE_DIR, FILEUPLOAD_URL, RESULTIMAGE_DIR, PRINTVIDEO_URL, FRAME_PATH,
BACKGROUND_PATH, VIDEO_PATH, SETTING_DIR, ASSETS_DIR, PASSWORD_URL, COMMON_URL;

FILEUPLOAD_URL = "https://api.hashsnap.net/upload/file";

//  개발자용
if (env) {
  PHOTO_FRAME_URL = `${ path.resolve('./src/renderer/assets/photo-frame.png').replace(/\\/gi,'/') }`;
  MYIMAGE_DIR = `${ path.resolve('./src/renderer/assets/photos').replace(/\\/gi,'/') }/`;
  RESULTIMAGE_DIR = "./src/renderer/assets/img/";
  PRINTVIDEO_URL = ".src/renderer/assets/video-0.mp4";
  FRAME_PATH = "./src/renderer/assets/setting/frames/";
  BACKGROUND_PATH = "./src/renderer/assets/setting/backgrounds/";
  VIDEO_PATH ="./src/renderer/assets/setting/video/"
  SETTING_DIR = "./src/renderer/assets/setting/";
  ASSETS_DIR = "./src/renderer/assets/";
  PASSWORD_URL = './src/renderer/common/password';
  COMMON_URL = './src/renderer/common/';
} else {
  PHOTO_FRAME_URL = `${ path.resolve('./assets/photo-frame.png').replace(/\\/gi,'/') }`;
  MYIMAGE_DIR = `${ path.resolve('./assets/photos').replace(/\\/gi,'/') }/`;
  RESULTIMAGE_DIR = `${ path.resolve('./assets/img/').replace(/\\/gi,'/') }/`;
  PRINTVIDEO_URL = `${ path.resolve('./assets/video-0.mp4').replace(/\\/gi,'/') }/`;
  FRAME_PATH = `${ path.resolve('./assets/setting/frames/').replace(/\\/gi,'/') }/`;
  BACKGROUND_PATH = `${ path.resolve('./assets/setting/backgrounds/').replace(/\\/gi,'/') }/`;
  VIDEO_PATH = `${ path.resolve('./assets/setting/video/').replace(/\\/gi,'/') }/`;
  SETTING_DIR = `${ path.resolve('./assets/setting/').replace(/\\/gi,'/') }/`;
  ASSETS_DIR = `${ path.resolve('./assets/').replace(/\\/gi,'/') }/`;
  PASSWORD_URL = `${ path.resolve('./common/password').replace(/\\/gi,'/') }/`
  COMMON_URL = `${ path.resolve('./common').replace(/\\/gi,'/') }/`
}
export {
  PHOTO_FRAME_URL, 
  MYIMAGE_DIR, 
  FILEUPLOAD_URL, 
  RESULTIMAGE_DIR, 
  PRINTVIDEO_URL, 
  FRAME_PATH,
  BACKGROUND_PATH,
  VIDEO_PATH,
  SETTING_DIR,
  ASSETS_DIR,
  PASSWORD_URL,
  COMMON_URL,
}


//  빌드용
// export const PHOTO_FRAME_URL = `${ path.resolve('./assets/photo-frame.png').replace(/\\/gi,'/') }`;
// export const MYIMAGE_DIR = `${ path.resolve('./assets/photos').replace(/\\/gi,'/') }/`;
// export const RESULTIMAGE_DIR = `${ path.resolve('./assets/img/').replace(/\\/gi,'/') }/`;
// export const FILEUPLOAD_URL = 'https://api.hashsnap.net/upload/file';
// export const PRINTVIDEO_URL = `${ path.resolve('./assets/video-0.mp4').replace(/\\/gi,'/') }/`;
// export const FRAME_PATH = `${ path.resolve('./assets/setting/frames/').replace(/\\/gi,'/') }/`;
// export const BACKGROUND_PATH = `${ path.resolve('./assets/setting/backgrounds/').replace(/\\/gi,'/') }/`;
// export const VIDEO_PATH = `${ path.resolve('./assets/setting/video/').replace(/\\/gi,'/') }/`;
// export const SETTING_DIR = `${ path.resolve('./assets/setting/').replace(/\\/gi,'/') }/`;
// export const ASSETS_DIR = `${ path.resolve('./assets/').replace(/\\/gi,'/') }/`;
// export const PASSWORD_URL = `${ path.resolve('./common/password').replace(/\\/gi,'/') }/`

//통합용
// export const PHOTO_FRAME_URL = `${ path.resolve('./src/renderer/assets/photo-frame.png').replace(/\\/gi,'/') }`;
// export const MYIMAGE_DIR = `${ path.resolve('./src/renderer/assets/photos').replace(/\\/gi,'/') }/`;

// export const RESULTIMAGE_DIR = 
// env ? "./src/renderer/assets/img/" : `${ path.resolve('./assets/img/').replace(/\\/gi,'/') }/`;

// export const FILEUPLOAD_URL = "https://api.hashsnap.net/upload/file";

// export const PRINTVIDEO_URL = 
// env ? "src/renderer/assets/video-0.mp4" : `${ path.resolve('./assets/video-0.mp4').replace(/\\/gi,'/') }/`;;
// export const FRAME_PATH = "./src/renderer/assets/setting/frames/";
// export const BACKGROUND_PATH = "./src/renderer/assets/setting/backgrounds/";
// export const VIDEO_PATH ="./src/renderer/assets/setting/video/"
// export const SETTING_DIR = "./src/renderer/assets/setting/";
// export const ASSETS_DIR = "./src/renderer/assets/";
// export const PASSWORD_URL = './src/renderer/common/password';