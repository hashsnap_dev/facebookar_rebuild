import { RESULTIMAGE_DIR, PASSWORD_URL, MYFICTURE_DIR } from '@/common/config'

const execSync = require('child_process').execSync;
const exec = require('child_process').exec;
const fs = require('fs');
const path = require('path');
const env = process.env.NODE_ENV == 'development' ? true : false;


export const adbExec = function(saveList) {
  const CONNECT_COMMAND = 'adb devices';
  const DEVICE_COMMAND = 'adb shell ls /storage/self/primary/DCIM/Camera -r';
  const DEVICEPULL_COMMAND = 'adb pull /storage/self/primary/DCIM/Camera/';
  // const MYFICTURE_DIR = ' D:/adbFicture/';
  let MYFICTURE_DIR = ` ${ path.resolve('./src/renderer/assets/photos') }`;
  if(!env) MYFICTURE_DIR = ` ${ path.resolve('./assets/photos') }`
  let FourList = [];
  let imgList = [];
  let realFictureList = [];
  try{
    //if(execSync(CONNECT_COMMAND).split('\n').length<1) return FourList;
    // realFictureList = execSync('dir D:\\adbFicture\\ /b').toString().split("\n"); // 로컬 파일 저장소 이름목록 불러오기
    realFictureList = execSync(`dir ${ MYFICTURE_DIR } /b`).toString().split("\n"); // 로컬 파일 저장소 이름목록 불러오기
    // copyFictureDelete(realFictureList, 'D:\\adbFicture\\');
    imgList = execSync(DEVICE_COMMAND).toString().split("\n"); // 안드로이드 사진 최신목록 불러와서 저장
    imgList = imgList.slice(0, imgList.length - 1); //  맨끝에 매번 공백이 하나 들어가서 제거
    if (imgList.length > 0) {
      if(imgList.length < 6) FourList = imgList.slice(0,imgList.length);
      else FourList = imgList.slice(0,6); // 불러올 개수
      if(saveList == FourList) return;
      frameImageDelete();
      for (let i = 0; i < FourList.length; i++){
        if (realFictureList.indexOf(FourList[i]) < 0){ // 로컬파일과 최신목록 비교
          execSync(`${ DEVICEPULL_COMMAND }${ FourList[i] }${ MYFICTURE_DIR.replace(/\\/gi,'/') }/`) // 로컬로 파일 복사
        }
      }
    }
    return FourList;
  }catch (err) {
    // console.log(err);
    return [];
  }
}
export const connectCheck = function() {
  const CONNECT_COMMAND = 'adb devices';
  const result = execSync(CONNECT_COMMAND).toString().split("\n");
  return result.length <= 3 ? false : true;
}

export const dirFile = function() { //  비연결 디버깅용
  let TEST_DIR = 'C://Users/user/Pictures/testFolder';
  TEST_DIR = "D://testFolder/";
  let check_list = [];
  fs.readdirSync(TEST_DIR).forEach((fileName) => {
    check_list.push(fileName.toString());
  })
  return check_list;
}

export const imageFileWrite = function(canvas) {
  const now = new Date();
  const realImgName = `${ now.getFullYear() }${ now.getMonth()+1 }${ now.getDate() }${ now.getHours() }${ now.getUTCMinutes() }.png`;
  const data = canvas.toDataURL('image/png').replace(/^data:image\/png;base64,/, '');
  fs.writeFileSync(RESULTIMAGE_DIR + realImgName, data, 'base64', err => {
  console.log(err);
  });
  return realImgName;
}

const copyFictureDelete = function(fictures, DIR) {
  const fictureLength = fictures.length;
  const limit = 20;
  if (fictureLength > limit) { // 만약 파일개수가 일정 개수 이상이라면.
    for(let delIdx = 0; delIdx < fictureLength-limit; delIdx++){
      exec(`del ${ DIR }${ fictures[delIdx] }`);
    }
  }
}

const frameImageDelete = function() { //프레임씌운 이미지 삭제용
  let frameList = [];
  let dir = "";
  dir = path.resolve(RESULTIMAGE_DIR);
  const limit = 1000; //  파일저장개수
  frameList = execSync(`dir ${ dir } /b`).toString().split("\n"); // 로컬 파일 저장소 이름목록 불러오기
  const listLength = frameList.length;
  // for (let i = 0; i < limit - listLength; i++) {
  //   exec(`del ${ dir }${ frameList[i] }`);
  // }
}

export const readJson = function() {  // 파일읽기
  return fs.readFileSync('./src/renderer/common/setting.json');
}
export const readPassword = function() {
  const passArray = fs.readFileSync(PASSWORD_URL);
  const password = String.fromCharCode(passArray[0],passArray[1],passArray[2],passArray[3]);
  return password;
}
