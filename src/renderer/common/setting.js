
import { FRAME_PATH, BACKGROUND_PATH, SETTING_DIR, VIDEO_PATH, PASSWORD_URL } from '../../common/config'

const fs = require('fs');
const fse = require('fs-extra');


export const writeJson = function(data) {
  const now = new Date();
  const realJsonName = `${ now.getFullYear() }-${ now.getMonth()+1 }-${ now.getDate() }-${ now.getHours() }-${ now.getUTCMinutes() }-${ now.getUTCSeconds() }.json`;
  const JSON_DIR = `${ SETTING_DIR }${ realJsonName }`;
  console.log(JSON_DIR);
  fs.writeFile(JSON_DIR, JSON.stringify(data), () => {
    console.log('세팅완료');
    exportSetting();
  })
}
export const writePassword = function(password) {
  fs.writeFile(PASSWORD_URL, password, () => {
    console.log('비번세팅완료')
  })
}

export const copyFile = function(dir, type) {
  let path = '';
  if (type == 'frame') path = FRAME_PATH;
  else if (type == 'video') path = VIDEO_PATH;
  else path = BACKGROUND_PATH;
  const fileSplit = dir.split('\\');
  try {
    fs.copyFileSync(dir, `${ path }${ fileSplit[fileSplit.length-1] }`);
  }catch(e) {
  }
  
}

export const readJson = function(dir) {  // 파일읽기
  
  // importSetting(dir, () => {
  //   return fs.readFileSync(dir);
  // });
  importSetting(dir)
  return fs.readFileSync(dir); 
}

const exportSetting = function() {
  const PATH = 'C:\\setting';
  
  fse.copy(SETTING_DIR, PATH, (err) => {
    console.log('export success');
  })
}
const importSetting = function(dir, callback) {

  let importPath = dir.split('\\'); // 요기는 세팅 이미지 프레임 등 다른것도 불러오는것
  let resultPath = '';
  
  for (let i = 0; i < importPath.length - 1 ; i++) {
    resultPath += importPath[i]+'\\';
  }
  fse.copy(resultPath, SETTING_DIR, (err) => {
    console.log('import success', resultPath);
    // callback();
  })
}