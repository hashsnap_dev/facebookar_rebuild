const fse = require('fs-extra');
const path = require('path');
function exportSetting() {
  const fromCommonDir = path.resolve('./src/renderer/common/').replace(/\\/gi,'/');
  const fromAssetsDir = path.resolve('./src/renderer/assets/').replace(/\\/gi,'/');
  const toCommonDir = path.resolve('./build/win-unpacked/common/').replace(/\\/gi,'/');
  const toAssetsDir = path.resolve('./build/win-unpacked/assets/').replace(/\\/gi,'/');


  fse.copy(fromCommonDir, toCommonDir, err => {
    console.log('common export success', err);
    fse.copy(fromAssetsDir, toAssetsDir, err => {
      console.log('assets export success', err);
    })
  })

}
exportSetting();